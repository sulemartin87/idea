<!doctype html>
<?php 
session_start();
if (isset($_SESSION["user_name"])) 
	{
		$user_name = $_SESSION["user_name"];
	}
else {
	$user_name = 'null';
}
?>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>

    <style>
        .demo-card-wide.mdl-card {
            width: 100%;
          
        }

        .demo-card-wide>.mdl-card__title {
            height: 176px;
            background-color: #42ebf4;
        }
		  body {
    background-image: url('images/bg_2880.jpg');
  }
    </style>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title --><span class="mdl-layout-title">Ideas Home</span>
                <!-- Add spacer,
        to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <!-- Navigation. We hide it in small screens. -->
                <nav class="mdl-navigation mdl-layout--large-screen-only"><a class="mdl-navigation__link" href="index.php">home</a>
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
              <?php if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a> <a class="mdl-navigation__link" href="departments.php">statistics</a>';} ?>
			  <?php if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                   
                        <?php if($user_name=='null') {
            echo'login';
        }

        else {
            echo'logout';
        }

        ?>
                    </a>
                </nav>
            </div>
        </header>
               <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
        </div>
        <main class="mdl-layout__content ">
            <div class="page-content mdl-gen mdl-cell mdl-cell--12-col">
                <!-- log in error is placed here -->
                <?php if($user_name=='null') {
            ?>
                <div class="demo-container mdl-grid">
                    <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
                        <h3>You are not Logged In, click on the log in <a href="login.php">link</a>to log in to the system</h3>
                    </div>
                </div>
                <!-- place content here -->
                <?php
        }

        else {
            ?>
                    <div class="demo-container mdl-grid">
                        <!-- <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>-->
                        <?php function load_table() {
                include 'database.php';
                if (mysqli_connect_errno()) {
                    echo "MySQLi Connection was not established: " . mysqli_connect_error();
                }
                else {
                    $final1="SELECT * FROM `idea` INNER JOIN users ON idea.User_ID = users.User_ID ORDER BY date_time DESC";
                    $run_user_all=mysqli_query($con, $final1);
                    $i=1;
                    while ($row=$run_user_all->fetch_assoc()) {
                       ?>
					   

                        <div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet">
                            <div class="demo-card-wide mdl-card mdl-shadow--8dp">
                                <div class="mdl-card__title" style="background-image: url('images/road_big.jpg');">
                                    <h2 class="mdl-card__title-text" style="color:white;">
                                        <?php print $row["Idea_Title"];
                        ?>
                                    </h2>
                                </div>
                                <div class="mdl-card__supporting-text">
                                    <form action="new entry.php" method="GET">
                                        <?php print $row["Idea_Body"];
                        echo '<input type="hidden" name="id" value="'.$row["Idea_ID"].'">
						<input type="hidden" name="Firstname" value="'.$row["Firstname"].'">
						<input type="hidden" name="Surname" value="'.$row["Surname"].'">';
                        ?></div>
                                <div class="mdl-card__actions mdl-card--border"></div><input type="submit" value="view" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"></form>
                               
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            load_table();

            
            ?>
                    </div>
                    <?php
        }

        ?>
            </div>
        </main>
    </div>
</body>

</html>