<!doctype html>
<?php
	session_start();

	?>
<html>
	<head>
		<title>University Idea Center</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Welcome to travelmw.com">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		<style>
			.demo-card-square.mdl-card 
			{
			overflow:hidden;
			margin:auto;
			margin-top:1%;
			}
			.demo-card-square > .mdl-card__title 
			{
			color: #fff;
			}
			body 
			{
			background-color:#EFEFEF;
			}
			ul 
			{	
			overflow: hidden;
			}
			li 
			{	
			float:left;
			}
			.mdl-textfield__input 
			{
			text-align:center;
			}
			.demo-card-square > .mdl-card__title 
			{
			width:100%;
			color: #009688;
			}
			.mdl-layout__drawer
			{
			background: rgb(217, 245, 249);
			}
		</style>
	</head>
	<body>
		<!-- Uses a header that scrolls with the text, rather than staying
			locked at the top -->
		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header  mdl-layout--fixed-tabs" >
			<header class="mdl-layout__header">
				<div class="mdl-layout__header-row">
					<span class="mdl-layout-title"><a class="mdl-navigation__link" href="index.php" style="font-size: 20px;">University Idea Center</span></a>
				</div>
			</header>
			<main class="mdl-layout__content">
				<div class="page-content">
					<!-- Your content goes here -->
					<!-- Square card -->
					<div class="page-content">
						<!-- Your content goes here --> 
						<div class="demo-card-square mdl-card mdl-shadow--2dp" style="padding:10px;">
							<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
								<div class="mdl-tabs__tab-bar">
									
									<a href="#sign_up_tab" class="mdl-tabs__tab is-active">Add User</a>
								</div>
							<br/>
							<div class="mdl-tabs__panel is-active" id="sign_up_tab">
									<form action="#" method="POST">
									<div>
									<select name="user_type">
								<option value="student">Student</option>
								<option value="QA Manager">QA Manager</option>
								<option value="QA Coordinator">QA Coordinator</option>
							
							</select>
									</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" id="first_name" required="required" name="first_name">
											<label class="mdl-textfield__label" for="first_name">First Name</label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" id="last_name" required="required" name="last_name">
											<label class="mdl-textfield__label" for="last_name">Last Name</label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" id="email" required="required" name="email">
											<label class="mdl-textfield__label" for="email">email</label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" >
											<input class="mdl-textfield__input" type="password" id="pass" required="required" name="pass">
											<label class="mdl-textfield__label" for="pass">password</label>
										</div>
										<input class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" type="submit" name="Add User" value="Sign Up"/>
									</form>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
							<?php

								if (isset($_POST['email']))
									{
										include 'database.php';
										$use = "Select * FROM users WHERE Email_Address='" . $_POST['email'] . "' LIMIT 1";
										if (mysqli_connect_errno())
										{
											echo "MySQLi Connection was not established: " . mysqli_connect_error();
										}
										else
										{
											$run_user = mysqli_query($con, $use);
											if (mysqli_num_rows($run_user) > 0)
											{
												$message = "email is already in use";
												echo "<script type='text/javascript'>alert('$message');</script>";
											}
											else
											{
												if (isset($_POST['first_name']))
												{
													$_SESSION['first_name'] = $_POST['first_name'];
													if (isset($_POST['last_name']))
													{
														$_SESSION['last_name'] = $_POST['last_name'];
														if (isset($_POST['email']))
														{
															$_SESSION['user_email'] = $_POST['email'];
															if (isset($_POST['pass']))
															{
																$_SESSION['user_pass'] = $_POST['pass'];
																$_SESSION['user_type'] = $_POST['user_type'];
																// header('Location: signup.php');

																echo ("<script>location.href = 'signup.php';</script>");
															}
														}
													}
												}
											}
										}
								}

							?>
		</div>
		</div>
		</div>
		</section>
		</div>
		</main>
		</div>
	</body>
</html>