<!doctype html>
<?php 
session_start();
ob_start();
if (isset($_SESSION["user_name"])) 
	{
		$user_name = $_SESSION["user_name"];
	}
else {
	$user_name = 'null';
}
?>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <link rel="stylesheet" href="styles.css">
    
</head>

<body>
<div class="demo-blog demo-blog--blogpost mdl-layout mdl-layout--fixed-header is-upgraded-">
        <header class="mdl-layout__header mdl-layout__header--waterfall">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">Ideas Home</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <!-- Navigation. We hide it in small screens. -->
                <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
                </nav>
            </div>
        </header>

        <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
        </div>


        <main class="mdl-layout__content">
            <div class="page-content">
                <!-- Your content goes here -->
            </div>
            <!-- log in error is placed here -->
            <?php 
            
            if($user_name == 'null') 
            {
              
            ?>

            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>You are not Logged In, click on the log in <a href="login.php">link</a> to log in to the system</h3>

                </div>
            </div>

            <!-- place content here -->
          <?php 
          }
          else 
          {
            
          
          ?>


<?php
	
		include 'database.php';
		if (mysqli_connect_errno())
		{
			echo "MySQLi Connection was not established: " . mysqli_connect_error();
		}
		else
		{	
			$id = $_GET["id"];
			
			echo '<input type="hidden" name="user_name" value="'.$user_name.'" id="user_name"></input>';
			echo '<input type="hidden" name="idea_id" value="'.$id.'" id="idea_id"></input>';
			
			$final1 = "SELECT * FROM `idea` INNER JOIN users ON idea.User_ID = users.User_ID WHERE `Idea_ID` = '".$id."'";
			$run_user_all = mysqli_query($con, $final1);
			$i = 1;
			while ($row = $run_user_all->fetch_assoc())

			{
				
			?>
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card mdl-shadow--4dp mdl-cell mdl-cell--12-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
			<h3><?php echo $row["Idea_Title"]; 
		 ?>
	</h3>
            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text meta">
              <div class="minilogo"></div>
              <div>

                <strong><?php print $row["Firstname"];  print " " .$row["Surname"];

								
                        ?></strong>
                <span><?php
					$date = $row["date_time"];
				
				
				
				function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

echo time_elapsed_string($date); 
		 ?></span>
              </div>
              <div class="section-spacer"></div>
			
              <div >
                		<?php
								  
								  $result = $con->query("SELECT COUNT(`Like_Type`) FROM `thumbs` WHERE `Like_Type` = '1' AND `Idea_ID`='".$id."'");
					$row1 = $result->fetch_row();
					echo '<p id="tum">'.$row1[0];' </p>';
					echo '<input type="hidden" name="idea_id" value="'.$row1[0].'" id="t_up"></input>';
			  
			  ?>  <button class="mdl-button mdl-js-button mdl-button--icon" onclick="sendmsg()">
 <i class="material-icons">thumb_up</i>
  <input type="hidden" name="thumbs_up" value="thumbs_up"></input>
</button>

              </div>
			   
			   <input type="hidden" name="thumbs_down" value="thumbs_down"></input>
              <div ><?php
								  
								  $result = $con->query("SELECT COUNT(`Like_Type`) FROM `thumbs` WHERE `Like_Type` = '2' AND `Idea_ID`='".$id."'");
					$row2 = $result->fetch_row();
					echo '<p id="tdd">'.$row2[0];' </p>';
					echo '<input type="hidden" name="idea_id" value="'.$row2[0].'" id="t_down"></input>';
			  
			  ?>
			  <button class="mdl-button mdl-js-button mdl-button--icon"  onclick="sendmsg2()">
  <i class="material-icons" >thumb_down</i>
</button>

               <!-- 500 <i class="material-icons" role="presentation">thumb_down</i>
                <span class="visuallyhidden">thumbs down</span>-->
              </div>

            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text">
              <p>
              
		<?php echo $row["Idea_Body"]; } ?>
			  
			  </p>
			  
			                <p>
              
		<u>Supporting Documnets </u>
			  
			  </p>
			 <?php 
			$date = date("Y");
								$name = $_GET['Firstname']. " " . $_GET['Surname'];
                               // echo "New record created successfully. Last inserted ID is: " . $last_id;
                                $directory = "uploads/".$date."/".$name."/".$_GET["id"]."/";
								$phpfiles = glob($directory . "*.{docx,DOCX,DOC,doc,PDF,pdf}", GLOB_BRACE);
foreach ($phpfiles as $phpfile)
{
    echo "<a href=\"$phpfile\">$phpfile></a>";
}
	echo '<input type="hidden" name="target" value="'.$directory.'" id="target"></input>'; 
?>


			<button class="mdl-button mdl-js-button mdl-button--raised mdl-button" onclick="sendmsg3()" >
  Zip All Data
</button>
            </div>
            <div class="mdl-color-text--primary-contrast mdl-card__supporting-text comments">
            <form action="comment.php" method="GET" >
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                  <textarea rows=1 class="mdl-textfield__input" id="comment" name="comment" id="field"></textarea>
                  <input type="hidden" name="id" value="<?php echo $id ?>"></input>
                  <label for="comment" class="mdl-textfield__label">Join the discussion</label>
                </div> 
               
				<p style="color:black;">Post as Anonymous?</p><input type="checkbox" id="anonymous" name="anonymous" class="mdl-checkbox__input" value="anonymous"/> 
				 <input type="submit" value="comment" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
				
              </form>
			  
				<?php 
				
        if (mysqli_connect_errno()) {
          echo "MySQLi Connection was not established: " . mysqli_connect_error();
      }
      else {
          $final1="SELECT * FROM `comment` INNER JOIN users ON comment.User_ID = users.User_ID WHERE `Idea_ID` = '".$id."' ORDER BY date_time DESC ";
          $run_user_all=mysqli_query($con, $final1);
          $i=1;
          while ($row=$run_user_all->fetch_assoc()) {
				
				?>
              <div class="comment mdl-color-text--grey-700">
                <header class="comment__header">
                  <img src="images/co1.jpg" class="comment__avatar">
                  <div class="comment__author">
                    <strong><?php print $row["Firstname"];  print " " .$row["Surname"];
                        ?></strong>
                    <!--<span>2 days ago</span>-->
					<br/>
                  </div>
                </header>
                <div class="comment__text">
                <?php print $row["Comment_Text"];
                        ?>
                </div>
              <!--  <nav class="comment__actions">
                  <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                    <i class="material-icons" role="presentation">thumb_up</i><span class="visuallyhidden">like comment</span>
                  </button>
                  <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                    <i class="material-icons" role="presentation">thumb_down</i><span class="visuallyhidden">dislike comment</span>
                  </button>
                  <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                    <i class="material-icons" role="presentation">share</i><span class="visuallyhidden">share comment</span>
                  </button>
                </nav>-->

              </div>
			  <div class="section-spacer"></div>
		  <?php }
	
			
	
		  ?>
            </div>
          </div>



          
          <?php 
        
          }
		  
		                   $id = $_GET["id"];
              /*  if (isset($_GET['thumb_up']))
                {
                    $comment = $_GET['comment'];
                    $sql = "INSERT INTO `likes` (`like_id`, `like_type`, `user_id`, `idea_id`) VALUES (null, '1', '1', '1');";	
                    if ($con->query($sql) === TRUE) {
                        echo "comment has been posted";
                    }
                    else{
                        echo "failed to post comment";
                    }
                   
                }*/
          }
		  }
          
          ?>
		  <p id="demo"></p>
	<script type="text/javascript">

		var user_name = document.getElementById("user_name").value;
		var idea_id = document.getElementById("idea_id").value;
		 
		  function sendmsg() {
		var xmlhttp=new XMLHttpRequest();
		var res =  xmlhttp.responseText; 
			like_type = 1;
		//	var tu = document.getElementById("t_up").value;
		  //var newtu = parseInt(tu) + 1;
		  // document.getElementById("tum").innerHTML = newtu;

	      xmlhttp.open("GET","like.php?like="+like_type+"&user_id=" + user_name + "&idea_id=" + idea_id,true);
		 console.log(xmlhttp.response);
      console.log(xmlhttp.responseXML);
	      xmlhttp.send();
		  
  		

}

		  function sendmsg2() {
		var xmlhttp=new XMLHttpRequest();
			like_type = 2;

	      xmlhttp.open("GET","like.php?like="+like_type+"&user_id=" + user_name + "&idea_id=" + idea_id,true);
	      xmlhttp.send();
		//  var td = document.getElementById("thumb_down").value;
		///var newtd = td + 1;
		//var tu = document.getElementById("thumb_up").value;
		  //var newtu = parseInt(tu) + 1;
		  // document.getElementById("tum").innerHTML = newtu;
  	

}

		  function sendmsg3() {
		var xmlhttp=new XMLHttpRequest();
		var res =  xmlhttp.responseText; 
			var target =  document.getElementById('target').value;

	      xmlhttp.open("GET","zip.php?directory="+target+"&user_name="+user_name,true);
		 console.log(xmlhttp.response);
      console.log(xmlhttp.responseXML);
	      xmlhttp.send();
		  
  		

}




function myFunction() {
    document.getElementById("demo").innerHTML = "Hello World";
}
</script>
        </main>
    </div>

</body>
<?php

$id = $_GET["id"];
$name = $_GET['Firstname']. " " . $_GET['Surname'];
$date = date("Y");
$target_dir = "uploads/".$date."/".$name."/".$id."/";

if (!file_exists($target_dir)) {
									mkdir($target_dir, 0777, true);
									file_put_contents($target_dir . 'idea.html', ob_get_contents());
// end buffering and displaying page
ob_end_flush();
								}else {
									file_put_contents($target_dir . 'idea.html', ob_get_contents());
								}

unset($_SESSION["target"]);


?>
</html>