<!doctype html>
<?php
session_start();

if (isset($_SESSION["user_name"]))
	{
	$user_name = $_SESSION["user_name"];
	}
  else
	{
	$user_name = 'null';
    } 
?>
<!-- Wide card with share menu button -->
<style>
    .demo-card-wide.mdl-card {
        margin: auto;
        margin-top: 1%;
        background-color: #e9f7e1;
    }
</style>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">Ideas Home</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <!-- Navigation. We hide it in small screens. -->
           <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
            </div>
        </header>

        <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
        </div>


        <main class="mdl-layout__content">
            <div class="page-content">
                <!-- Your content goes here -->
            </div>
            <!-- log in error is placed here -->
            <?php if($user_name=='null' ) { ?>

            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col "></div>
                <div class="demo-content mdl-color--white mdl-shadow--8dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>You are not Logged In, click on the log in <a href="login.php">link</a> to log in to the system</h3>

                </div>
            </div>

            <!-- place content here -->
            <?php } else { ?>

          <form action="upload idea.php" method="post" enctype="multipart/form-data">
            <div class="demo-card-wide mdl-card mdl-shadow--8dp">
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text">Add New Idea</h2>
                </div>
                <div class="mdl-card__supporting-text" style="width:93%;">
                <p>Select Categories</p>

                <select name="cat" style="margin:auto;width:98%;" >
                <?php
                include 'database.php';

                if (mysqli_connect_errno())
                    {
                    echo "MySQLi Connection was not established: " . mysqli_connect_error();
                    }
                else
                    {
                    $final1 = "SELECT * FROM `category`";
                    $run_user_all = mysqli_query($con, $final1);
                    $i = 1;
                    while ($row = $run_user_all->fetch_assoc())
                        {
                        ?>
                                        <option value="<?php echo $row["Category_ID"]; ?>"><?php echo $row["Category_Name"] ?></option>
                         <?php               
                        }
                    } 
                ?>
                    </select>
                </div>
                <div class="mdl-card__supporting-text">
                    <!-- Floating Multiline Textfield -->
                    
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="title" name="title">
                            <label class="mdl-textfield__label" for="title">Idea Title</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield">
                            <textarea class="mdl-textfield__input" type="text" rows="3" id="body" name="body"></textarea>
                            <label class="mdl-textfield__label" for="body">Idea Body</label>
                        </div>

                        <input type="file" name="fileToUpload" id="fileToUpload">
                        
                        <div>



                            <input type="checkbox" id="check" name="check" class="mdl-checkbox__input" value="check" required/> accept terms and conditions
                            <div class="mdl-card__actions mdl-card--border"></div>
                            <input type="submit" value="submit" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                        </div>
                    </form>
                </div>
            </div>

            <?php } ?>
        </main>
    </div>

</body>



</html>