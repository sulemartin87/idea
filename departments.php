<!doctype html>
<?php 
 include 'database.php';
session_start();
if (isset($_SESSION["user_name"])) 
	{
		$user_name = $_SESSION["user_name"];
		
	}
else {
	$user_name = 'null';
}
?>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">Departments</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
                </nav>


            </div>
        </header>

<div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
        </div>


        <main class="mdl-layout__content">
            <div class="page-content">
                <!-- Your content goes here -->
            </div>
            <!-- log in error is placed here -->
            <?php if($user_name == 'null') {?>

            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>You are not Logged In Click on the log in link to log in to the system</h3>

                </div>
            </div>

            <!-- place content here -->
            <?php }
	else {
		
	
	?>
            <div class="mdl-layout-spacer"></div>
            <div class="demo-container mdl-grid">
                <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Department Name</th>
							<th>QA Coordinator</th>
							<th>number of ideas in department</th>
							<th>Percentage</th>
							<th>Unique Contributors</th>
							<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>
                                <!-- Textfield with Floating Label -->

                                <form action="" method="POST">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                                        <input class="mdl-textfield__input" type="text" id="add_category" name="add_category">
                                        <label class="mdl-textfield__label" for="add_category">Add Department</label>
								
                                    </div>

                            </td>

                            <td>		                <select name="cat" style="margin:auto;width:98%;" >
									<?php
									include 'database.php';

									if (mysqli_connect_errno())
										{
										echo "MySQLi Connection was not established: " . mysqli_connect_error();
										}
									else
										{
										$final1 = "SELECT * FROM `users`";
										$run_user_all = mysqli_query($con, $final1);
										$i = 1;
										while ($row = $run_user_all->fetch_assoc())
											{
											?>
															<option value="<?php echo $row["User_ID"]; ?>"><?php echo $row["Firstname"]." " . $row["Surname"] ?></option>
											 <?php               
											}
										} 
									?>
                    </select></td>							<td>
							</td><td></td><td></td><td><input class=" mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-color--green" type="submit" value="+" name="category" onclick="return confirm(&#39;Add new category?&#39;)" />
							</td>
                            </form>
                        </tr>
                        <tr>
                        <?php
                        function load_table() {
                            include 'database.php';
                            if (mysqli_connect_errno())
                            {
                                echo "MySQLi Connection was not established: " . mysqli_connect_error();
                            }
                            else
                            {	
                                $final1 = "SELECT * FROM `department` INNER JOIN users ON `department`.`User_ID` = users.`User_ID`";
                                $run_user_all = mysqli_query($con, $final1);
                                $i = 1;
                                while ($row = $run_user_all->fetch_assoc())

			                    {

                                    
                                    $category_id = $row["Department_ID"]; 
                                    $category_name = $row["Firstname"]." ". $row["Surname"];
									 $department_name = $row["Department_Name"];
									 $department_id = $row["Department_ID"];
									 
									 									$count ="SELECT COUNT(Idea_ID) FROM `idea` INNER JOIN category ON category.Category_ID = idea.Category_ID INNER JOIN department ON category.Department_ID = 
																		department.Department_ID WHERE department.`Department_ID` = '".$department_id."'
";

									 									$count5 ="SELECT COUNT(DISTINCT idea.User_ID) FROM `idea` INNER JOIN category ON category.Category_ID = idea.Category_ID
INNER JOIN department ON category.Department_ID = department.Department_ID 
WHERE department.`Department_ID` = '".$department_id."'
";


									 									$count2 ="SELECT COUNT(Idea_ID) FROM `idea`
INNER JOIN category ON category.Category_ID = idea.Category_ID
INNER JOIN department ON category.Department_ID = department.Department_ID";
									 									?>
                                    
                                    <form action="" method="POST">
                                        <td class="mdl-data-table__cell--non-numeric"><?php echo $department_name; ?></td>
										<td class="mdl-data-table__cell--non-numeric"><?php echo  $category_name;?></td>
										<td class="mdl-data-table__cell-numeric"><?php
										$result = $con->query($count);
										$row3 = $result->fetch_row();
					echo $row3[0];  ?></td>
															<td class="mdl-data-table__cell--numeric"><?php
										$result1 = $con->query($count2);
										$row4 = $result1->fetch_row();
										if ($row4[0] == 0) {
											echo '0%';
										}else {
											echo ($row3[0]/$row4[0])*100 . "%";
										}
					 ?></td>
					
															<td class="mdl-data-table__cell--numeric"><?php
										$result2 = $con->query($count5);
										$row5 = $result2->fetch_row();
					echo $row5[0];  ?></td>
					
                                        <td><!--<input class=" mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-color--red" type="submit" value="X" <?php echo 'name="bruh' . $row["Department_ID"] . '"';?>  onclick="return confirm(&#39;Delete category?&#39;)" /> --></td>

                                   
                                    </form>   
                                    </tr>

                                    <?php

                                        if (isset($_POST['bruh' . $row["Department_ID"]])) 
                                        {

                                            if (isset($category_id))
                                            {
                                                $remove_sql = "DELETE FROM `category` WHERE `category`.`Category_ID` = ".$category_id."";	
                                                $remove = mysqli_query($con, $remove_sql);
                                                echo '<script>
                                                function myFunction() {
                                                    location.reload();
                                                }
                                                </script>';
                                            }
                                            
                                        }

                                    ?>

                        <?php        
                                    $i = $i++;    
                                }
                            }
                        }

                        load_table();

                        ?>

                    </tbody>
                </table>
            </div>
            <?php

                        

                        if (mysqli_connect_errno())
                        {
                            echo "MySQLi Connection was not established: " . mysqli_connect_error();
                        }
                        else
                        {	
                                    if (isset($_POST['category'])) 
                                    {

                                        if (isset($_POST['add_category']))
                                        {
                                            $remove_sql = "INSERT INTO `category` (`Category_ID`, `Department_ID`, `Category_Name`) VALUES (NULL, '".$_POST['cat']."', '".$_POST['add_category']."');";
											
                                            $remove = mysqli_query($con, $remove_sql);
                                           
                                        }
                                       
                                    }
  
                        }
                    }
            ?>

        </main>
    </div>

</body>

</html>