<?php

session_start();

?>
<html>
<head>
	<title>travelmw</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Welcome to travelmw.com.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
   <style>
		.demo-card-square.mdl-card 
		{
		  width: 400px;
		  height: 400px;
		  margin:auto;
		  margin-top:1%;
		  
		}
		.demo-card-square > .mdl-card__title 
		{
		  color: #fff;
		}

		body 
		{
			background-color:#EFEFEF;
		}
		ul 
		{	
		  overflow: auto;
		}
		li 
		{	
			 float:left;
		}

		.mdl-textfield__input 
		{
			text-align:center;
		}
		.demo-card-square > .mdl-card__title 
		{
			width:100%;
			color: #009688;
		}
		.mdl-layout__drawer
		{
			background: rgb(217, 245, 249);
		}
		
		#view-source 
		{
		  position: fixed;
		  display: block;
		  right: 0;
		  bottom: 0;
		  margin-right: 40px;
		  margin-bottom: 40px;
		  z-index: 900;
		}

	</style>
  
</head>

<body>

<!-- Uses a header that scrolls with the text, rather than staying
  locked at the top -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    
	<div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title"><a class="mdl-navigation__link" href="index.php" style="font-size: 20px;">University Idea Centre</span></a>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation -->
      <nav class="mdl-navigation">

	  <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu">
	  <li class="mdl-menu__item"><a href="login.php" class="mdl-menu__item">Logout</a></li>
	  </ul>
     
	 </nav>
    
	</div>
	
  </header>
  
  

<main class="mdl-layout__content">

<div class="page-content">
  <!-- Your content goes here -->
  
<?php
// establishing the MySQLi connection
if(isset($_SESSION['first_name']))
{
 if(isset($_SESSION['last_name']))
	{
	   if(isset($_SESSION['user_email']))
		{
			if(isset($_SESSION['user_pass']))
			{
			
			$pass = $_SESSION['user_pass'];
			//$pass = md5($pass);
			//$con = mysqli_connect("localhost","root","","travelmw");
			include 'database.php';
			//$sq = "INSERT INTO `travelmw`.`users` (`user_id`, `user_name`, `user_pass`, `full_name`) VALUES (NULL, '".$_SESSION['email']."', '".$_SESSION['pass']."', '".$_SESSION['name']."');";
			//$sq = "INSERT INTO `idea`.`users` (`user_id`, `first_name`, `last_name`, `user_name`, `user_pass`, `user_email`) VALUES (NULL, '".$_SESSION['first_name']."', '".$_SESSION['last_name']."', '".$_SESSION['user_email']."', '".$pass."', '".$_SESSION['user_email']."');";
			//$sq ="INSERT INTO `users` (`user_pass`, `user_type`, `user_name`, `first_name`, `last name`) VALUES ('', 'student', '', '', '');";
			$sq = "INSERT INTO `users` (`User_ID`, `Firstname`, `Surname`, `Email_Address`, `Password`, `User_Type`,`Role`) VALUES (NULL, '".$_SESSION['first_name']."', '".$_SESSION['last_name']."', '".$_SESSION['user_email']."', '".$pass."', '".$_SESSION['user_type']."', '');";
			if (mysqli_connect_errno())

			{
				echo "MySQLi Connection was not established: " . mysqli_connect_error();
			}

			else
			{
			
				echo '<div class="demo-card-square mdl-card mdl-shadow--2dp" style=" height: 500px; margin:auto;">
				<div class="mdl-card__title">
				<h2 class="mdl-card__title-text">  Thank You for joining the idea centre
				</br>
				</br>
				</br>
				</br>
				</h2>
				</br>
				</div>
				</br>
				</br>
				<div>
				<span class="mdl-layout-title"><a class="mdl-navigation__link" href="login.php" style="font-size: 20px; margin-left:30%;">Please Log in Now</span></a><br/><br/>
				</div>
				</div>';

				if (mysqli_query($con, $sq)) 
				{
					$username = $_SESSION['user_email'];
					unset($_SESSION['user_email']);
					unset($_SESSION['first_name']);
					unset($_SESSION['last_name`']);
					unset($_SESSION['user_pass`']);
					
					
				} 
				else 
				{
					unset($_SESSION['user_email']);
					unset($_SESSION['first_name']);
					unset($_SESSION['last_name`']);
					unset($_SESSION['user_pass`']);
					
				}
				
				
		
			}
		}
		}
	}
}
else 
{
	echo '<div class="demo-card-square mdl-card mdl-shadow--2dp" style=" height: 500px; margin:auto;">
			<div class="mdl-card__title">
			<h2 class="mdl-card__title-text">  
			</br>
			</br>
			</br>
			Sorry. Something went wrong 
			</br>
			</h2>
			</br>
			</div>
			</br>
			</br>
			<div>
			<span class="mdl-layout-title"><a class="mdl-navigation__link" href="index.php" style="font-size: 20px; margin-left:30%;">Go Home</span></a><br/><br/>
			</div>
			</div>';
  
}
 
?>
 
 </div>
 
</main>
</div>

</body>
</html>