<!doctype html>
<?php 
session_start();
if (isset($_SESSION["user_name"])) 
	{
		$user_name = $_SESSION["user_name"];
	}
else {
	$user_name = 'null';
}
?>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">Ideas Home</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <!-- Navigation. We hide it in small screens. -->
                <nav class="mdl-navigation mdl-layout--large-screen-only">
                    <a class="mdl-navigation__link" href=""></a>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
                </nav>
            </div>
        </header>

        <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <!--<a class="mdl-navigation__link" href="">Link</a>-->
                <a class="mdl-navigation__link" href="login.php">
                    <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                </a>
            </nav>
        </div>


        <main class="mdl-layout__content">
            <div class="page-content">
                <!-- Your content goes here -->
            </div>
            <!-- log in error is placed here -->
            <?php 
            
            if($user_name == 'null') 
            {
              
            ?>

            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>You are not Logged In, click on the log in <a href="login.php">link</a> to log in to the system</h3>

                </div>
            </div>

            <!-- place content here -->
          <?php 
          }
          else 
          {
            
          
          ?>











          
          <?php 
          }
          
          ?>
        </main>
    </div>

</body>

</html>