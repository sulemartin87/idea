<?php


$directory = $_GET["directory"];
$user_name = $_GET["user_name"];

function zipfolder($directory, $user_name) {
    $zip = new ZipArchive;
$date = date("Y");
/*if ($zip->open('test_dir.zip', ZipArchive::OVERWRITE) === TRUE)
{
    if ($handle = opendir('uploads'))
    {
        // Add all files inside the directory
        while (false !== ($entry = readdir($handle)))
        {
			$dir = "uploads/".$date;
            if ($entry != "." && $entry != ".." && !is_dir($dir . $entry))
            {
				
                $zip->addFile($dir. $entry);
            }
        }
        closedir($handle);
    }
 
    $zip->close();
}*/


// Get real path for our folder
$rootPath = realpath($directory);

// Initialize archive object
$zip = new ZipArchive();
$zip->open($user_name.'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();
}


zipfolder($directory, $user_name);
?>