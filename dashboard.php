<!doctype html>
<?php 
 include 'database.php';
session_start();
if (isset($_SESSION["user_name"])) 
	{
		$user_name = $_SESSION["user_name"];
		
	}
else {
	$user_name = 'null';
}
?>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> University Idea Center</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">Categories</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                                <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
                </nav>

                <!-- Navigation. We hide it in small screens.-->

            </div>
        </header>
		
		        <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="index.php">home</a>
                
                <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <?php  if($_SESSION["User_Type"] == 'QA Manager') {echo'<a class="mdl-navigation__link" href="dashboard.php">categories</a>';} ?>
			  <?php ["User_Type"]; if($_SESSION["User_Type"] == 'admin') {echo'<a class="mdl-navigation__link" href="admin.php">Admin Panel</a>';} ?>
                    <a class="mdl-navigation__link" href="login.php">
                        <?php if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                    </a>
            </nav>
        </div>


      <!--  <div class="mdl-layout__drawer">
            <span class="mdl-layout-title">Categories</span>
            <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="index.php">home</a>
            <a class="mdl-navigation__link" href="add idea.php">add idea</a>
                <a class="mdl-navigation__link" href="dashboard.php">categories</a>
                <a class="mdl-navigation__link" href="">Link</a>
                <a class="mdl-navigation__link" href="login.php">
                    <?php// if($user_name == 'null') {echo'login';} else {echo'logout';} ?>
                </a>
            </nav>
        </div>-->


        <main class="mdl-layout__content">
            <div class="page-content">
                <!-- Your content goes here -->
            </div>
            <!-- log in error is placed here -->
            <?php if($user_name == 'null') {?>

                       <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>You are not Logged In, click on the log in <a href="login.php">link</a> to log in to the system</h3>

                </div>
            </div>

            <!-- place content here -->
            <?php }
	else {
	if ($_SESSION["User_Type"] == 'QA Manager'){
	
	?>
            <div class="mdl-layout-spacer"></div>
            <div class="demo-container mdl-grid">
                <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Category Name</th>
							<th>Department</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>
                                <!-- Textfield with Floating Label -->

                                <form action="" method="POST">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                                        <input class="mdl-textfield__input" type="text" id="add_category" name="add_category">
                                        <label class="mdl-textfield__label" for="add_category">Add Category</label>
								
                                    </div>

                            </td>
                            <td>		                <select name="cat" style="margin:auto;width:98%;" >
									<?php
									include 'database.php';

									if (mysqli_connect_errno())
										{
										echo "MySQLi Connection was not established: " . mysqli_connect_error();
										}
									else
										{
										$final1 = "SELECT * FROM `department`";
										$run_user_all = mysqli_query($con, $final1);
										$i = 1;
										while ($row = $run_user_all->fetch_assoc())
											{
											?>
															<option value="<?php echo $row["Department_ID"]; ?>"><?php echo $row["Department_Name"] ?></option>
											 <?php               
											}
										} 
									?>
                    </select></td><td><input class=" mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-color--green" type="submit" value="+" name="category" onclick="return confirm(&#39;Add new category?&#39;)" />
							</td>
                            </form>
                        </tr>
                        <tr>
                        <?php
                        function load_table() {
                            include 'database.php';
                            if (mysqli_connect_errno())
                            {
                                echo "MySQLi Connection was not established: " . mysqli_connect_error();
                            }
                            else
                            {	
                                $final1 = "SELECT * FROM `category` INNER JOIN department ON `category`.`Department_ID` = department.Department_ID";
                                $run_user_all = mysqli_query($con, $final1);
                                $i = 1;
                                while ($row = $run_user_all->fetch_assoc())

			                    {
                                    
                                    $category_id = $row["Category_ID"]; 
                                    $category_name = $row["Category_Name"];
									 $department_id = $row["Department_Name"];?>
                                    
                                    <form action="" method="POST">
                                        <td class="mdl-data-table__cell--non-numeric"><?php echo $category_name; ?></td>
										<td class="mdl-data-table__cell--non-numeric"><?php echo $department_id; ?></td>
                                        <td><input class=" mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-color--red" type="submit" value="X" <?php echo 'name="bruh' . $row["Category_ID"] . '"';?>  onclick="return confirm(&#39;Delete category?&#39;)" /> </td>

                                   
                                    </form>   
                                    </tr>

                                    <?php

                                        if (isset($_POST['bruh' . $row["Category_ID"]])) 
                                        {

                                            if (isset($category_id))
                                            {
                                                $remove_sql = "DELETE FROM `category` WHERE `category`.`Category_ID` = ".$category_id."";	
                                                $remove = mysqli_query($con, $remove_sql);
                                                echo '<script>
                                                function myFunction() {
                                                    location.reload();
                                                }
                                                </script>';
                                            }
                                            
                                        }

                                    ?>

                        <?php        
                                    $i = $i++;    
                                }
                            }
                        }

                        load_table();

                        ?>

                    </tbody>
                </table>
            </div>
            <?php

                        

                        if (mysqli_connect_errno())
                        {
                            echo "MySQLi Connection was not established: " . mysqli_connect_error();
                        }
                        else
                        {	
                                    if (isset($_POST['category'])) 
                                    {

                                        if (isset($_POST['add_category']))
                                        {
                                            $remove_sql = "INSERT INTO `category` (`Category_ID`, `Department_ID`, `Category_Name`) VALUES (NULL, '".$_POST['cat']."', '".$_POST['add_category']."');";
											
                                            $remove = mysqli_query($con, $remove_sql);
                                           
                                        }
                                       
                                    }
  
                        }
	} else {
		          echo '              <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                    <h3>Authentication Error, click on the log in <a href="login.php">link</a> to log in to the system with appropraite Credentials</h3>

                </div>
            </div>';
	}
                    }
            ?>

        </main>
    </div>

</body>

</html>